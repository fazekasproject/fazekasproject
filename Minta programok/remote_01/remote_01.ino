#include "IRLibAll.h"

//Create a receiver object to listen on pin 2
IRrecvPCI myReceiver(2);

//Create a decoder object
IRdecode myDecoder;

void setup()
{
  Serial.begin(9600);
  while (!Serial); //delay for Leonardo
  myReceiver.enableIRIn();
  Serial.println(F("Ready to receive IR signals"));
}

void loop()
{
  if (myReceiver.getResults())
  {
    myDecoder.decode();           //Decode it
    int val = myDecoder.value;
    if (val != 4294967295 && val != -1)
    {
      Serial.println(val);
    }
    myReceiver.enableIRIn();      //Restart receiver
  }
}
