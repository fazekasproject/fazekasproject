/*
 * 0 FF6897
 * 1 FF30CF
 * 2 ff18e7
 * 3 FF7A85
 * 4 FF10EF
 * 5 FF38C7
 * 6 FF5AA5
 * 7 FF42BD
 * 8 FF4AB5
 * 9 FF52AD
 * EQ FF906F
 */
#include "IRLibAll.h"

//Create a receiver object to listen on pin 2
IRrecvPCI myReceiver(2);

//Create a decoder object 
IRdecode myDecoder;   

void setup() {
  Serial.begin(9600);
//  delay(2000); while (!Serial); //delay for Leonardo
  myReceiver.enableIRIn(); // Start the receiver
  Serial.println(F("Ready to receive IR signals"));
}

void loop()
{
  if(myReceiver.getResults()) {
    myDecoder.decode();
//    if(myDecoder.protocolNum==UNKNOWN) {
       Serial.println(myDecoder.value,HEX);
//    } else {
//      myDecoder.dumpResults(false);
//    }
    myReceiver.enableIRIn(); 
  }
}
