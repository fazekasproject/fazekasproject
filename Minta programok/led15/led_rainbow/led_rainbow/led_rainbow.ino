int red=9;
int green=10;
int blue=11;

void setup()
{
  pinMode(red, OUTPUT);
  pinMode(green, OUTPUT);
  pinMode(blue, OUTPUT);

  Serial.begin(9600);
}

void loop()
{
  for (int r=0; r<=255; r++)
  {
    analogWrite (red, r);
    for (int g=0; g<=255; g++)
    {
      analogWrite (green, g);
       for (int b=0; b<=255; b++)
        {
          analogWrite (blue, b);
        }
    }
  }
}
