
int X_pin = 0;    // A0
int Y_pin = 1;    // A1
int Z_pin = 7;    // D7, only GND if pushed otherwise floating

int red_led_pin=10;
int green_led_pin=11;
int pot_value = 0; 

int val;

void setup() {

  pinMode(red_led_pin, OUTPUT);
  pinMode(green_led_pin, OUTPUT);
  pinMode(X_pin, INPUT);
  pinMode(Y_pin, INPUT);
  
  Serial.begin(9600);
}

void loop() {

int red_value, green_value;

    pot_value = analogRead(X_pin);
    red_value = pot_value*255/1024;
    green_value = 255 - red_value;
  
    analogWrite(red_led_pin, red_value);
    analogWrite(green_led_pin, green_value);

    delay(200); 

    Serial.print("Piros:");
    Serial.println(red_value, DEC);
    Serial.print("Zöld:");
    Serial.println(green_value, DEC);

}

