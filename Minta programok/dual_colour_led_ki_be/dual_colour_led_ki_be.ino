int red_led_pin=10;
int green_led_pin=11;

int val; 

void setup() {

  pinMode(red_led_pin, OUTPUT);
  pinMode(green_led_pin, OUTPUT);
  
  Serial.begin(9600);
}

void loop() {

  digitalWrite(red_led_pin,1);
  Serial.println("Piros bekapcsolva.");
  delay(150); 


  digitalWrite(red_led_pin,0);
  Serial.println("Piros kikapcsolva.");
  delay(150); 

  digitalWrite(green_led_pin,1);
  Serial.println("Zöld bekapcsolva.");
  delay(150); 

  digitalWrite(green_led_pin,0);
  Serial.println("Zöld kikapcsolva.");
  delay(150); 

}

