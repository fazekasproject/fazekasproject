
#include <stdio.h>
#include <DS1302.h>

// for LCD serial:
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

uint8_t date_time[7];

namespace {

//const int kCePin   = 8;  // Chip Enable
//const int kIoPin   = 7;  // Input/Output
//const int kSclkPin = 6;  // Serial Clock

  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);

  pinMode(4, INPUT);
  pinMode(5, INPUT);
  pinMode(6, INPUT);
  pinMode(7, INPUT);

// Create a DS1302 object.
//DS1302 rtc(kCePin, kIoPin, kSclkPin);
LiquidCrystal_I2C lcd(0x3F,16,2);  // set the LCD address to 0x27 for a 16 chars and 2 line display

String dayAsString(const uint8_t day) {
  switch (day) {
    case 1: return "Vasarnap";
    case 2: return "Hetfo";
    case 3: return "Kedd";
    case 4: return "Szerda";
    case 5: return "Csutortok";
    case 6: return "Pentek";
    case 7: return "Szombat";
  }
  return "(unknown day)";
}
int get_buttons()
{  
  
   for(int line=8; line<12; line++)
  {
     digitalWrite(line, 1);
     
     for(int col=4; col<8;col++)
     {//ZAR
//    if ( digitalRead(col) == 1 )
      if ( analogRead(col) > 800 )
         {//Sz
           button = line*10+col;
           return button;      
         }//Sz
     }//ZAR
     digitalWrite(line, 0);
  }
  return 0;
}
void ask_time(uint8_t date_time[]) {
   Serial.println("Ev [nn]:");
   while (Serial.available()==0)  {
   }
   date_time[0]=Serial.parseInt();                    // év beolvasása
   Serial.println("Ho [1-12]:");
   while (Serial.available()==0)  {
   }
   date_time[1]=Serial.parseInt();                    // hó beolvasása
   
   Serial.println("Nap [1-31]:");
   while (Serial.available()==0)  {
   }
   date_time[2]=Serial.parseInt();                    // nap beolvasása
   
   Serial.println("Ora [0-23]:");
   while (Serial.available()==0)  {
   }
   date_time[3]=Serial.parseInt();                    // óra beolvasása
   
   Serial.println("Perc[0-59]:");
   while (Serial.available()==0)  {
   }
   date_time[4]=Serial.parseInt();                    // perc beolvasása
   
   Serial.println("Mp [0-59]:");
   while (Serial.available()==0)  {
   }
   date_time[5]=Serial.parseInt();                    // mp beolvasása
   
   Serial.println("Het napja [1-7]:");
   while (Serial.available()==0)  {
   }
   date_time[6]=Serial.parseInt();                    // hét napja beolvasása
 
}
void printTime() {
  
  Time t = rtc.time();  // Get the current time and date from the chip.

  
  const String day = dayAsString(t.day);  // Name the day of the week.

  // Format the time and date and insert into the temporary buffer.
  char buf[50];
  snprintf(buf, sizeof(buf), "%s %04d-%02d-%02d %02d:%02d:%02d",
           day.c_str(),
           t.yr, t.mon, t.date,
           t.hr, t.min, t.sec);

  // Print the formatted string to serial so we can see the time.
  Serial.println(buf);
  lcd.print(buf);
 
}

//}  // namespace

void setup() {
  Serial.begin(9600);

  lcd.init();                      // initialize the lcd 
  lcd.backlight();
  lcd.clear();
/*
  // Initialize a new chip by turning off write protection and clearing the clock halt flag. These methods needn't always be called.
  rtc.writeProtect(false);
  rtc.halt(false);
 
  ask_time(date_time);

//  Time t(2017, 4, 3, 12, 13, 0, Time::kMonday); // Make a new time object to set the date and time. Sunday, September 22, 2013 at 01:38:50.
//  Time t(2017, 4, 3, 12, 13, 0, 2); // Make a new time object to set the date and time. Sunday, September 22, 2013 at 01:38:50.
  Time t(date_time[0]+2000, date_time[1], date_time[2], date_time[3], date_time[4], date_time[5], date_time[6]); // Make a new time object to set the date and time. Sunday, September 22, 2013 at 01:38:50.
  rtc.time(t);  // Set the time and date on the chip.
  */
}

// Loop and print the time every second.
void loop() {
  //printTime();

//  delay(1000);
  Serial.println(get_buttons());
}
